package by.itstep.collections.manager.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Table (name = "tag")
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany (mappedBy = "tags")
    private List<Collection> collections;

}
