package by.itstep.collections.manager.entity;

import java.sql.Date;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table (name = "comment")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String message;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user; // кто коммент оставил

    @EqualsAndHashCode.Exclude
    @ToString.Exclude

    @ManyToOne // множестов айтемов у каждй колелкции
    @JoinColumn(name = "collection_id")
    private Collection collection; // под кем комментарий

    @Column(name = "createAt", nullable = false, unique = false)
    private Date createdAt; //  дата создания коммента




}
