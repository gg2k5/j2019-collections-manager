package by.itstep.collections.manager.entity;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@Table (name = "collection")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Collection {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "name", nullable = false, unique = true)
    private String name;

    @Column (name = "titles", nullable = false, unique = false)
    private String titles;

    @Column (name = "description", nullable = true, unique = false)
    private String description;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Column (name = "imageUrl", nullable = true, unique = false)
    private String imageUrl;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany (mappedBy = "collection") // коллекция одна, но у неё много итемов
    private List<CollectionItem> items;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany (mappedBy = "collection")
    private List<Comment> comments;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn (name = "user_id")
    private User user;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable (name = "collection_tag" ,
            joinColumns = {@JoinColumn(name = "collection_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}    )
    private List<Tag> tags;
}
