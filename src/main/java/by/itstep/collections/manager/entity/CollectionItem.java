package by.itstep.collections.manager.entity;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Table (name = "collectionitem")
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CollectionItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne // множестов айтемов у каждй колелкции
    @JoinColumn(name = "collection_id")
    private Collection collection;

    @Column (name = "name", nullable = false, unique = true)
    private String name;


}
