package by.itstep.collections.manager.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Table (name = "`user`")
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class User {

    @Column(name = "name")
    private String name;

    @Column (name = "email", nullable = false, unique = true)
    private String email;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "lastName")
    private String lastName;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany (mappedBy = "user")
    private List<Collection> collectionList;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany (mappedBy = "user")
    private List<Comment> comments;

    @Column(name = "role")
    private Role role; // ENUM

}
