package by.itstep.collections.manager;

import by.itstep.collections.manager.util.EntityManagerUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

		EntityManagerUtil.getEntityManager();
	}


}
